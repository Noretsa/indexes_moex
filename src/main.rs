use colored::*;
use std::fs::File;
use std::io::{self, BufRead, Write};
use std::path::Path;
use std::*;

static PATH_TO_BALANCE: &str = "./src/balance.txt";

static URL_MOEX: &str = "https://iss.moex.com/iss";
static MOEX_INDEX: &str = "/statistics/engines/stock/markets/index/analytics/moexbc.json?limit=100";
static BOND_INDEX: &str =
    "/statistics/engines/stock/markets/index/analytics/RUGBITR5Y.json?limit=100";
static MOEX_INFO_1: &str = "/engines/stock/markets/shares/boardgroups/57/securities/";
static MOEX_INFO_2: &str =
    "?iss.only=securities,marketdata&securities.columns=LOTSIZE,ISIN&marketdata.columns=LAST";
static BOND_INFO_1: &str = "/engines/stock/markets/bonds/boardgroups/58/securities/";
static BOND_INFO_2: &str =
    "?iss.only=securities,marketdata&securities.columns=SECID,SHORTNAME,ISIN&marketdata.columns=LAST";

#[derive(Debug, Clone, Copy)]
enum InstrumentType {
    Stock,
    Bond,
}

#[derive(Debug, Clone)]
struct Instrument {
    isin: String,
    ticker: String,
    shortname: String,
    instrument_type: InstrumentType,
    lot: i32,
    in_index: bool,
    weight: f32,
    last: f32,
    balance: i32,
}
impl Instrument {
    fn get_instrument(&mut self) -> Result<(), anyhow::Error> {
        let url_stock_moex_info = format!("{}{}", URL_MOEX, MOEX_INFO_1);
        let url_bond_moex_info = format!("{}{}", URL_MOEX, BOND_INFO_1);
        let url = match self.instrument_type {
            InstrumentType::Stock => {
                format!(
                    "{}{}{}{}",
                    url_stock_moex_info,
                    self.ticker.as_str(),
                    ".json",
                    MOEX_INFO_2
                )
            }
            InstrumentType::Bond => {
                format!(
                    "{}{}{}{}",
                    url_bond_moex_info,
                    self.ticker.as_str(),
                    ".json",
                    BOND_INFO_2
                )
            }
        };
        let client = reqwest::blocking::Client::new();
        let request = match client.get(url).send()?.text() {
            Ok(s) => s,
            Err(e) => panic!(
                "Запрос данных по тикеру {} не выполнен из-за ошибки: {}",
                self.ticker, e
            ),
        };
        let response: serde_json::Value = serde_json::from_str(&request)?;
        let arr_stock = response["securities"]["data"][0].as_array().unwrap();
        // println!("{:?}", arr_stock);

        let last = response["marketdata"]["data"][0][0].as_f64().unwrap() as f32;
        match self.instrument_type {
            InstrumentType::Stock => {
                let lot = arr_stock[0].as_i64().unwrap() as i32;
                // println!("lot {}, last {}", lot, last);
                self.isin = arr_stock[1].as_str().unwrap().to_owned();
                self.lot = lot;
                self.last = last;
            }
            InstrumentType::Bond => {
                self.isin = arr_stock[2].as_str().unwrap().to_owned();
                self.shortname = arr_stock[1].as_str().unwrap().to_owned();
                self.ticker = arr_stock[0].as_str().unwrap().to_owned();
                self.last = last;
                self.lot = 1;
            }
        }

        Ok(())
    }
}

struct InstrumentsVector {
    data: Vec<Instrument>,
}
impl InstrumentsVector {
    fn new() -> InstrumentsVector {
        let url_index = format!("{}{}", URL_MOEX, MOEX_INDEX);

        // let url_stock_moex_info = format!("{}{}", URL_MOEX, MOEX_INFO_1);

        let mut instruments: Vec<Instrument> = Vec::new();
        match InstrumentsVector::get_index_data(&url_index, InstrumentType::Stock) {
            Ok(s) => {
                s.into_iter().for_each(|stock| instruments.push(stock));
            }
            Err(e) => panic!("Не получили вектор акций: {}", e),
        };
        let url_index = format!("{}{}", URL_MOEX, BOND_INDEX);
        match InstrumentsVector::get_index_data(&url_index, InstrumentType::Bond) {
            Ok(s) => {
                s.into_iter().for_each(|stock| instruments.push(stock));
            }
            Err(e) => panic!("Не получили вектор облигаций: {}", e),
        };

        for instr in &mut instruments {
            match Instrument::get_instrument(instr) {
                Ok(()) => {}
                Err(e) => panic!("Ошибка при get_instrument() {}", e),
            }
        }

        if let Ok(lines) = read_file(PATH_TO_BALANCE) {
            for line in lines {
                if let Ok(balance) = line {
                    let vec_balance: Vec<&str> = balance.split(";").collect();
                    // println!(
                    //     "type: {}, ticker: {}, balance: {}",
                    //     vec_balance[0], vec_balance[1], vec_balance[2]
                    // );
                    let instr_type = vec_balance[0].to_owned();
                    let ticker = vec_balance[1].to_owned();
                    let balance: i32 = vec_balance[2].trim().parse().unwrap();
                    let instrument_type = match instr_type.as_str() {
                        "stock" => InstrumentType::Stock,
                        "bond" => InstrumentType::Bond,
                        _ => panic!("Неизвестный тип у тикера: {}", ticker),
                    };
                    let mut i = Instrument {
                        isin: String::new(),
                        ticker,
                        shortname: vec_balance[1].to_string(),
                        instrument_type,
                        lot: 1,
                        in_index: false,
                        weight: 0.0,
                        last: 0.0,
                        balance,
                    };
                    match Instrument::get_instrument(&mut i) {
                        Ok(()) => {}
                        Err(e) => {
                            panic!("Ошибка при get_instrument(): {} на тикер: {}", e, i.ticker)
                        }
                    }
                    match instruments.iter_mut().find(|item| *item.ticker == i.ticker) {
                        Some(p) => {
                            p.balance = i.balance;
                        }
                        _ => instruments.push(i),
                    }
                }
            }
        }
        Self { data: instruments }
    }

    fn get_index_data(
        url: &str,
        instr_type: InstrumentType,
    ) -> Result<Vec<Instrument>, anyhow::Error> {
        let mut v: Vec<Instrument> = Vec::new();
        let client = reqwest::blocking::Client::new();
        let request = match client.get(url).send()?.text() {
            Ok(s) => s,
            Err(e) => panic!(
                "Запрос индекса {:?} не выполнен из-за ошибки: {}",
                instr_type, e
            ),
        };
        let response: serde_json::Value = serde_json::from_str(&request)?;
        let arr_index = response["analytics"]["data"].as_array().unwrap();
        for ticker in arr_index {
            let new_instr = Instrument {
                isin: String::new(),
                ticker: ticker[2].as_str().unwrap_or("Нет тикера!").to_owned(),
                shortname: ticker[2].as_str().unwrap_or("Нет тикера!").to_owned(),
                instrument_type: instr_type,
                lot: 1,
                in_index: true,
                weight: ticker[5].as_f64().unwrap() as f32,
                last: 0.0,
                balance: 0,
            };
            v.push(new_instr);
        }
        Ok(v)
    }
}

#[derive(Debug)]
struct Shares {
    data: Vec<Instrument>,
    money_in_shares: f32,
}
impl Shares {
    fn new() -> Shares {
        let data: Vec<Instrument> = Vec::new();
        let money_in_shares = 0.0;
        Shares {
            data,
            money_in_shares,
        }
    }
}

#[derive(Debug)]
struct Bonds {
    data: Vec<Instrument>,
    money_in_bonds: f32,
}

impl Bonds {
    fn new() -> Bonds {
        let data: Vec<Instrument> = Vec::new();
        let money_in_bonds = 0.0;
        Bonds {
            data,
            money_in_bonds,
        }
    }
}

fn show_shares(papers: &Vec<Instrument>, money_for_buy: f32) {
    for stock in papers {
        // println!("{:?}", stock);
        let mut my_weight = (stock.last * stock.balance as f32) / money_for_buy;
        //     let need_buy = (account_money * (stock.weight / 100.)) / stock.lotprice;
        let mut need_buy =
            (money_for_buy * (stock.weight as f32 / 100.)) / (stock.last * stock.lot as f32);
        let mut lotprice = stock.last * stock.lot as f32;

        let color = match stock.balance > 0 && !stock.in_index {
            true => colored::Color::BrightRed,
            _ => match stock.balance == 0 && stock.in_index {
                true => colored::Color::BrightCyan,
                _ => colored::Color::BrightGreen,
            },
        };
        match stock.instrument_type {
            InstrumentType::Stock => {
                println!("{:-<89}", "");
            }
            InstrumentType::Bond => {
                println!("{:-<95}", "");
                lotprice *= 10.;
                need_buy /= 10.;
                my_weight *= 10.;
            }
        }

        let sss = format!(
            "| {:6} | {:12} | {:8} | {:10.2} | {:5}% | {:6.1} | {:8} | {:7.2}% |",
            stock.ticker.yellow(),
            stock.shortname,
            stock.last,
            lotprice,
            stock.weight,
            // need_buy - (stock.balance / stock.lot as i32) as f32,
            need_buy.trunc() as i32 - (stock.balance / stock.lot as i32),
            stock.balance / stock.lot as i32,
            my_weight * 100.,
        );
        println!("{}", sss.color(color));
    }
}

fn main() {
    let instruments = InstrumentsVector::new();
    let mut shares = Shares::new();
    let mut bonds = Bonds::new();

    for stock in &instruments.data {
        match stock.instrument_type {
            InstrumentType::Stock => {
                shares.data.push(stock.clone());
                shares.money_in_shares += stock.last * stock.balance as f32;
            }
            InstrumentType::Bond => {
                bonds.data.push(stock.clone());
                bonds.money_in_bonds += stock.last * (stock.balance as f32 * 10.);
            }
        }
    }
    print!("\nКакую сумму необходимо распределить? Введите сумму: ");
    io::stdout().flush().unwrap();
    let mut deposit = String::new();
    io::stdin()
        .read_line(&mut deposit)
        .expect("Failed to read line!");
    let deposit: f32 = deposit.trim().parse().expect("Please type a number!");
    let q = format!(
        "\nКакой процент облигаций будет в портфеле? (От 0 до 100. Если указать 100 - \
              покажет только таблицу облигаций, если 0 - покажет только таблицу \
              акций, иначе распределит деньги между индексами): "
    );
    print!("{}", q.yellow());
    io::stdout().flush().unwrap();
    let mut percent_bonds = String::new();
    io::stdin()
        .read_line(&mut percent_bonds)
        .expect("Failed to read line!");
    let percent_bonds: i32 = percent_bonds.trim().parse().expect("Please type a number!");
    let all_money = deposit + shares.money_in_shares + bonds.money_in_bonds;
    // let mut money_for_bonds = 0.0;
    // let mut money_for_shares = 0.0;
    let head = format!(
        "\nАкции\n{:-<89}\n| {:6} | {:12} | {:8} | {:10} | {:5}% | {:6} | {:8} | {:7}% |",
        "", "Ticker", "Shortname", "Цена", "Цена (lot)", "Вес ", "Купить", "Куплено", " %"
    );
    let head_bond = format!(
        "\nОблигации\n{:-<95}\n| {:12} | {:12} | {:8} | {:10} | {:5}% | {:6} | {:8} | {:7}% |",
        "", "Ticker", "Shortname", "%Номинал", "Цена", "Вес ", "Купить", "Куплено", " %"
    );

    match percent_bonds {
        1..=99 => {
            let money_for_bonds = all_money * (percent_bonds as f32 / 100.);
            let money_for_shares = all_money - money_for_bonds;
            println!("{}", head.blue());
            show_shares(&shares.data, money_for_shares);
            println!(
                "{:-<89}\n\t\tВсего денег в акциях: {} ",
                "", shares.money_in_shares
            );
            println!("\n{}", head_bond.blue());
            show_shares(&bonds.data, money_for_bonds);
            println!(
                "{:-<95}\n\t\tВсего денег в облигациях: {} ",
                "", bonds.money_in_bonds
            );
            // println!(
            //     "\t\tОстаток на счете: {} ",
            //     deposit - shares.money_in_shares - bonds.money_in_bonds
            // );
        }
        0 => {
            println!("{}", head.blue());
            show_shares(&shares.data, all_money);
            println!(
                "{:-<89}\n\t\tВсего денег в акциях: {} ",
                "", shares.money_in_shares
            );
        }
        100 => {
            println!("{}", head_bond.blue());
            show_shares(&bonds.data, all_money);
            println!(
                "{:-<95}\n\t\tВсего денег в облигациях: {} ",
                "", bonds.money_in_bonds
            );
        }
        _ => {
            println!("Неправильно введен процент облигаций в портфеле. Попробуйте еще раз.");
        }
    }
}

fn read_file<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
